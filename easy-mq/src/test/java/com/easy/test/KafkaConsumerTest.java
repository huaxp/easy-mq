package com.easy.test;

import java.util.Arrays;
import java.util.Properties;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.TopicPartition;
/**
 * KafkaMessageListenerContainer
 * @author root
 *
 */
public class KafkaConsumerTest {

	public static void main(String[] args) {

		Properties props = new Properties();

		props.put("bootstrap.servers", "192.168.1.11:9092");

		props.put("group.id", "test1");

		props.put("enable.auto.commit", "false");
		
		props.put("default.topic", "test-topic");
		
		props.put("retries","10");
		
		props.put("auto.offset.reset", "earliest");

		props.put("auto.commit.interval.ms", "1000");
		
		props.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");

		props.put("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");

		KafkaConsumer consumer = new KafkaConsumer<>(props);

		consumer.subscribe(Arrays.asList("platform-command"));  

		boolean flag = true;

		while (flag) {
			ConsumerRecords<String,String> records = consumer.poll(1);

		
			for (ConsumerRecord record : records) {
				TopicPartition topicPartition = new TopicPartition(record.topic(), record.partition());
				System.out.printf("offset = %d, key = %s, value = %s%n", record.offset(), record.key(), record.value());
				//consumer.commitAsync();
				if(!"9".equals(record.value())) {
					consumer.seek(topicPartition, record.offset());
				}
			}
		}

		consumer.close();

	}
}