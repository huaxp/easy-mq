package com.easy.test.rocket;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.rocketmq.client.exception.MQClientException;
import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.common.message.Message;
import org.apache.rocketmq.remoting.common.RemotingHelper;
import org.springframework.util.SerializationUtils;

import com.easy.mq.client.RocketClient;
import com.easy.mq.config.producer.RocketProducerConfig;
import com.easy.mq.entry.JsonUtil;
import com.easy.mq.exception.MQException;
import com.easy.mq.result.RocketProducerMessage;
import com.easy.mq.result.RocketSendResult;
import com.reach.constrant.Topic;
import com.reach.dto.UserEventReq;
import com.reach.enums.EventType;

public class VisitRocketMQProducerTest {

	 public static void main(String[] args) throws MQException, MQClientException, UnsupportedEncodingException {
		RocketProducerConfig config = new RocketProducerConfig();
		//config.setAddress("39.98.195.15:9876");
		config.setAddress("39.98.195.15:9876");
		//config.setInstanceName("Producer");
		//config.setGroup("rocket");
		RocketProducerMessage message = new RocketProducerMessage();
		message.setTopic(Topic.ACTION_TOPIC);
		/*Map json = new HashMap<>();
		json.put("time",String.valueOf(System.currentTimeMillis()));
		json.put("age", "22");
		json.put("name", "张三");
		message.setBody(JsonUtil.bean2Json(json).getBytes());
		message.setTopic("topic2-test");
		System.out.println("json:"+JsonUtil.bean2Json(message));*/
		UserEventReq req = new UserEventReq();
		req.setEvent(EventType.VISIT);
		req.setDeviceId("ADCADFAF10001");
		req.setChannel("H5");
		message.setBody(SerializationUtils.serialize(req));
		 RocketClient client = new RocketClient(config);
		 
		RocketSendResult result =client.send(message,true);		
	
		System.out.println(result);

	/*
		 RocketProducerMessage message = new RocketProducerMessage();
		 JSONObject json = new JSONObject();
		 json.put("time",String.valueOf(System.currentTimeMillis()));
		 json.put("age", "22");
		 json.put("name", "张三");
		 message.setContent(json.toJSONString());
		 message.setKeys(String.valueOf(System.currentTimeMillis()));
		 MQSendResult result = MQHelp.build().send(message);
		System.out.println(result);*/
	
		/* DefaultMQProducer producer = new DefaultMQProducer("DEFAULT");//1.实例化
		    producer.setNamesrvAddr("39.98.195.15:9876");//2.设置namesrv
		    producer.start();//3.producer开始
		    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//设置日期格式
		    String currentTime=df.format(new Date());
		    Message msg = new Message("topic-1",// topic
                    currentTime.getBytes(RemotingHelper.DEFAULT_CHARSET));// body
			 for (int i = 0; i < 3; i++) {
		        try {
		                
		             
		                SendResult sendResult = producer.send(msg);
		                System.out.println(sendResult);

		        } catch (Exception e) {
		            e.printStackTrace();
		        }
			 }

		    producer.shutdown();
*/
	}
	
}
