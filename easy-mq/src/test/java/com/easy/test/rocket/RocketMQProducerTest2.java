package com.easy.test.rocket;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.apache.rocketmq.client.exception.MQClientException;
import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.common.message.Message;
import org.apache.rocketmq.remoting.common.RemotingHelper;

import com.easy.mq.client.RocketClient;
import com.easy.mq.config.producer.RocketProducerConfig;
import com.easy.mq.entry.JsonUtil;
import com.easy.mq.exception.MQException;
import com.easy.mq.result.RocketProducerMessage;
import com.easy.mq.result.RocketSendResult;

public class RocketMQProducerTest2 {

		 public static void main(String[] args) throws MQClientException {
			 for (int i = 0; i < 10; i++) {
				DefaultMQProducer producer = new DefaultMQProducer(UUID.randomUUID().toString());
				producer.setNamesrvAddr("39.98.195.15:9876");
				producer.setInstanceName("producer");
				producer.start();
				try {
					
						//Thread.sleep(5000); //每5秒发送一次MQ
						Message msg = new Message("mail_topic",// topic
						"TagA",// tag
						(new Date() + " Hello RocketMQ ,QuickStart" + i).getBytes());
						SendResult sendResult = producer.send(msg);
						System.out.println(sendResult);
					
					} catch (Exception e) {
					 e.printStackTrace();
				  }
				  //producer.shutdown();
			 }
		}
	
}
