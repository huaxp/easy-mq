package com.easy.test.rocket;

import java.util.List;

import org.apache.rocketmq.client.consumer.DefaultMQPushConsumer;
import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyContext;
import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyStatus;
import org.apache.rocketmq.client.consumer.listener.MessageListenerConcurrently;
import org.apache.rocketmq.client.exception.MQClientException;
import org.apache.rocketmq.common.message.MessageExt;

import com.easy.mq.client.RocketClient;
import com.easy.mq.config.consumer.RocketConsumerConfig;
import com.easy.mq.consumer.MQPushConsumer;
import com.easy.mq.listener.MQMessageListenerConcurrently;
import com.reach.constrant.Group;
import com.reach.constrant.Topic;

public class RocketMQConsumerTest {

	public static void main(String[] args) {

		 DefaultMQPushConsumer pushConsumer = new DefaultMQPushConsumer(Group.ACTION_GROUP);  
	        //pushConsumer.setNamesrvAddr("192.168.180.1:9876");  
	        pushConsumer.setNamesrvAddr("39.98.195.15:9876");  
	       // pushConsumer.setInstanceName("Producer");  
		
	        try {
				pushConsumer.subscribe(Topic.ACTION_TOPIC, "*");
			} catch (MQClientException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}  
		   pushConsumer.registerMessageListener(new MessageListenerConcurrently() {  
            @Override  
            public ConsumeConcurrentlyStatus consumeMessage(List<MessageExt> msgs, ConsumeConcurrentlyContext context) {  
                  MessageExt messageExt = msgs.get(0);  
                   System.out.println(new String(messageExt.getBody()));  
                  return ConsumeConcurrentlyStatus.CONSUME_SUCCESS;  
           }  
		});
		try {
			pushConsumer.start();
		} catch (MQClientException e) {
			e.printStackTrace();
		}

	}

}
