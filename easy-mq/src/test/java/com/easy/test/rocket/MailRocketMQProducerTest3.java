package com.easy.test.rocket;

import java.io.UnsupportedEncodingException;

import org.apache.rocketmq.client.exception.MQClientException;

import com.easy.mq.client.RocketClient;
import com.easy.mq.config.producer.RocketProducerConfig;
import com.easy.mq.exception.MQException;
import com.easy.mq.result.RocketProducerMessage;
import com.easy.mq.result.RocketSendResult;
import com.reach.constrant.Group;
import com.reach.constrant.Topic;
import com.reach.dto.MailReq;
import com.reach.enums.EmailType;

public class MailRocketMQProducerTest3 {

	 public static void main(String[] args) throws MQException, MQClientException, UnsupportedEncodingException {
		RocketProducerConfig config = new RocketProducerConfig();
		//config.setAddress("39.98.195.15:9876");
		config.setAddress("39.98.195.15:9876");
		//config.setInstanceName("Producer");
		//config.setGroup("rocket");
		
		/*Map json = new HashMap<>();
		json.put("time",String.valueOf(System.currentTimeMillis()));
		json.put("age", "22");
		json.put("name", "张三");
		message.setBody(JsonUtil.bean2Json(json).getBytes());
		message.setTopic("topic2-test");
		System.out.println("json:"+JsonUtil.bean2Json(message));*/
		
		
		RocketProducerMessage message = new RocketProducerMessage();
		message.setTopic("mail_topic");
		message.setGroup(Group.EMAIL_GROUP);
		MailReq req = new MailReq();
		req.setContent("test");
		req.setFrom("huaxp_cn@163.com");
		req.setTo(new String[]{"184409610@qq.com"});
		req.setSubject("test2");
		req.setType(EmailType.TEXT);
		message.setBody("helo".getBytes());
	RocketSendResult result =new RocketClient(config).send(message);		
	
		System.out.println(result);

	/*
		 RocketProducerMessage message = new RocketProducerMessage();
		 JSONObject json = new JSONObject();
		 json.put("time",String.valueOf(System.currentTimeMillis()));
		 json.put("age", "22");
		 json.put("name", "张三");
		 message.setContent(json.toJSONString());
		 message.setKeys(String.valueOf(System.currentTimeMillis()));
		 MQSendResult result = MQHelp.build().send(message);
		System.out.println(result);*/
	
		/* DefaultMQProducer producer = new DefaultMQProducer("DEFAULT");//1.实例化
		    producer.setNamesrvAddr("39.98.195.15:9876");//2.设置namesrv
		    producer.start();//3.producer开始
		    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//设置日期格式
		    String currentTime=df.format(new Date());
		    Message msg = new Message("topic-1",// topic
                    currentTime.getBytes(RemotingHelper.DEFAULT_CHARSET));// body
			 for (int i = 0; i < 3; i++) {
		        try {
		                
		             
		                SendResult sendResult = producer.send(msg);
		                System.out.println(sendResult);

		        } catch (Exception e) {
		            e.printStackTrace();
		        }
			 }

		    producer.shutdown();
*/
	}
	
}
