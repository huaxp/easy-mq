package com.easy.test;

import java.util.Properties;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;

public class KafkaProducerTest {

	public static void main(String[] args) {

		Properties props = new Properties();

		props.put("bootstrap.servers", "192.168.1.11:9092");

		props.put("acks", "all");

		props.put("retries", 0);

		props.put("batch.size", 16384);

		props.put("linger.ms", 1);

		props.put("buffer.memory", 33554432);

		props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");

		props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");

		Producer<String, String> producer = new KafkaProducer<>(props);

		/*JSONObject json = new JSONObject();
		json.put("simNo", "100001");
		json.put("message", "张三");*/
		
		Future<RecordMetadata> result = producer
				.send(new ProducerRecord("terminal-onlinestatus", "gateway1", "{\"simNo\":\"1\",\"state\":1}"));
		/*try {
			//System.out.println(JSONUtil.toJson(result.get()));
		} catch (InterruptedException | ExecutionException e) {
			e.printStackTrace();
		}*/
		try {
			System.out.println(result.get().hasOffset());
		} catch (InterruptedException | ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		producer.close();

	}

}
