package com.easy.mq.transition;

public interface ISerialization {
	
	public <T> T  deserializer(byte[] msg,Class<?> cl);
	
	public <T> T  deserializer(String msg,Class<?> cl);
	
	public byte[]  serialization(String content);
}
