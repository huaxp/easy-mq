package com.easy.mq.transition;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;

public class JackSon implements ISerialization{

	private static final Logger logger = LoggerFactory.getLogger(JackSon.class);
	
	
	@SuppressWarnings("unchecked")
	@Override
	public <T> T deserializer(byte[] msg, Class<?> cl) {
		try {
			ObjectMapper om = new ObjectMapper();
			return (T) om.readValue(msg, cl);
		}catch(Exception e) {
			logger.error(e.getMessage());
		}
		return null;
	}

	@Override
	public byte[] serialization(String content) {
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> T deserializer(String msg, Class<?> cl) {
		try {
			ObjectMapper om = new ObjectMapper();
			return (T) om.readValue(msg, cl);
		}catch(Exception e) {
			logger.error(e.getMessage());
		}
		return null;
	}
	
}
