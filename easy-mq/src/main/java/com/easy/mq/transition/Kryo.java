package com.easy.mq.transition;

import org.springframework.stereotype.Service;

@Service("KRYO")
public class Kryo implements ISerialization{

	@Override
	public <T> T deserializer(byte[] msg, Class<?> cl) {
		return null;
	}

	@Override
	public byte[] serialization(String content) {
		return null;
	}

	@Override
	public <T> T deserializer(String msg, Class<?> cl) {
		return null;
	}

}
