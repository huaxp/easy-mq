package com.easy.mq.transition;


public class SerializationFactory {

	public static ISerialization factory(String serializationName) {
		if(serializationName == null) {
			return null;
		}
		try {
			Object serialization =  Class.forName(serializationName).newInstance();
			if(serialization == null) {
				return null;
			}
			return (ISerialization) serialization;
		}catch(Exception e) {
			return null;
		}
	}
	
}
