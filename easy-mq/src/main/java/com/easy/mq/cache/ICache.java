package com.easy.mq.cache;

public interface ICache {
	
	public void delete(String key);
	
	public Long inc(String key,int expire);
	
	public Integer get(String key);

}
