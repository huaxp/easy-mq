package com.easy.mq.event;

import java.io.Serializable;

public class MQEvent<T> implements Serializable{
	
	private static final long serialVersionUID = -6428321214433962495L;
	private String key;
	private String topic;
	private String group;
	private T content;
	
	public String getTopic() {
		return topic;
	}
	public void setTopic(String topic) {
		this.topic = topic;
	}
	public String getGroup() {
		return group;
	}
	public void setGroup(String group) {
		this.group = group;
	}
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public T getContent() {
		return content;
	}
	public void setContent(T content) {
		this.content = content;
	}
	

}
