package com.easy.mq.event;

import java.util.List;

import com.easy.mq.enums.ConsumeResultStatus;

public interface MQConsumerListener {
	
	public ConsumeResultStatus onMQEvent(MQEvent<Object> event);
	
	public ConsumeResultStatus onMQEvent(List<MQEvent<Object>> event);
}
