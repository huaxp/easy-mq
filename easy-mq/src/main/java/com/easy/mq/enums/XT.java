package com.easy.mq.enums;

public enum XT {
    DEFAULT, DIRECT, TOPIC, HEADERS, FANOUT
}
