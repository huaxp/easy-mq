package com.easy.mq.listener;

import com.easy.mq.config.consumer.AbstractConsumerConfig;

public interface MessageListenerContainer {
	
	public void registerMessageListener(AbstractConsumerConfig consumerConfig);
}
