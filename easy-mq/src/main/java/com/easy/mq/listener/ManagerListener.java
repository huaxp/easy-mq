package com.easy.mq.listener;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.easy.mq.config.consumer.AbstractConsumerConfig;
import com.easy.mq.enums.ConsumeResultStatus;
import com.easy.mq.event.MQConsumerListener;
import com.easy.mq.event.MQEvent;
import com.fasterxml.jackson.databind.ObjectMapper;

public class ManagerListener<T> {

	private static final Logger logger = LoggerFactory.getLogger(ManagerListener.class);

	private  Map<String, List<MQConsumerListener>> annotationListener = new ConcurrentHashMap<String, List<MQConsumerListener>>();
	
	private List<MessageListenerContainer>  messageList  = new ArrayList<>();
	
	public  void addMQListener(String key, MQConsumerListener listener) {
		List<MQConsumerListener> list = annotationListener.get(key);
		if (list == null) {
			List<MQConsumerListener> newList = new ArrayList<MQConsumerListener>();
			newList.add(listener);
			annotationListener.put(key, newList);
		}
	}
	
	public  ConsumeResultStatus notifyListener(AbstractConsumerConfig consumerConfig,List<MQEvent<Object>> event) {
		List<MQConsumerListener> list = annotationListener.get(this.getKey(consumerConfig));
		for (final MQConsumerListener listener : list) {
			if(logger.isDebugEnabled()) {
				try {
					ObjectMapper obj = new ObjectMapper();
					logger.debug("Notify {} ",obj.writeValueAsString(event));
				}catch(Exception e) {
					logger.error(e.getMessage());
				}
			}
			try {
				return listener.onMQEvent(event);
			} catch (Throwable t) {
				logger.error("Notification  message error!", t);
			}finally{
			}
		}
		
		logger.error("No listener key : {}",this.getKey(consumerConfig));
		return ConsumeResultStatus.FAIL;
	}

	public  ConsumeResultStatus notifyListener(AbstractConsumerConfig consumerConfig,MQEvent<Object> event) {
		event.setGroup(consumerConfig.getGroup());
		event.setTopic(consumerConfig.getTopic());
		List<MQConsumerListener> list = annotationListener.get(this.getKey(consumerConfig));
		for (final MQConsumerListener listener : list) {
			if(logger.isDebugEnabled()) {
				try {
					ObjectMapper obj = new ObjectMapper();
					logger.debug("Notify {} ",obj.writeValueAsString(event));
				}catch(Exception e) {
					logger.error(e.getMessage());
				}
			}
			try {
				return listener.onMQEvent(event);
			} catch (Throwable t) {
				logger.error("Notification  message error!", t);
			}finally{
			}
		}
		
		logger.error("No listener key : {}",this.getKey(consumerConfig));
		return ConsumeResultStatus.FAIL;
	}
	
	public String getKey(AbstractConsumerConfig consumerConfig){
		StringBuilder sb = new StringBuilder();
		sb.append(consumerConfig.getListenerClassName());
		sb.append("_");
		sb.append(consumerConfig.getTargetMethod());
		return String.valueOf(sb);
	}

	public  List<MessageListenerContainer> getMessageList() {
		return messageList;
	}

	public void add(MessageListenerContainer messageListenerContainer) {
		this.messageList.add(messageListenerContainer);
	}

}
