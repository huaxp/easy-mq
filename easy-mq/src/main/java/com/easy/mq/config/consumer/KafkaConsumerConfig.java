package com.easy.mq.config.consumer;

import com.easy.mq.cache.ICache;

public class KafkaConsumerConfig extends AbstractConsumerConfig{
	
	private ICache  cache;
	
	private Integer expire;

	public Integer getExpire() {
		return expire;
	}

	public void setExpire(Integer expire) {
		this.expire = expire;
	}

	public ICache getCache() {
		return cache;
	}

	public void setCache(ICache cache) {
		this.cache = cache;
	}
	
}
