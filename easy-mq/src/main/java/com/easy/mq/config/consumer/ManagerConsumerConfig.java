package com.easy.mq.config.consumer;

import java.lang.annotation.Annotation;
import java.util.HashMap;
import java.util.Map;

public class ManagerConsumerConfig<T> {
	private Class<Annotation> Listener;
	
	private Map<String, Object> config = new HashMap<String, Object>();
	T consumer;
	
	public T getConsumer() {
		return consumer;
	}
	public void setConsumer(T consumer) {
		this.consumer = consumer;
	}
	public Class<Annotation> getListener() {
		return Listener;
	}
	public void setListener(Class<Annotation> listener) {
		Listener = listener;
	}

	
	public Map<String, Object> getConfig() {
		return config;
	}
	public void setConfig(Map<String, Object> config) {
		this.config = config;
	}
	
	
}
