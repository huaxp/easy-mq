package com.easy.mq.config.consumer;

import java.util.HashMap;
import java.util.Map;

public class AbstractConsumerConfig {

	private String topic;
	private String group;
	
	private String paramType;
	
	private String parentType;
	private String listenerClassName;
	private String errorHandler;
	private String targetMethod;
	
	private int timeout = 500;
	
	private boolean autoCommit;
	
	private int retries;
	
	private String transfer;
	
	
	public String getTransfer() {
		return transfer;
	}
	public void setTransfer(String transfer) {
		this.transfer = transfer;
	}
	public boolean isAutoCommit() {
		return autoCommit;
	}
	public void setAutoCommit(boolean autoCommit) {
		this.autoCommit = autoCommit;
	}
	public int getRetries() {
		return retries;
	}
	public void setRetries(int retries) {
		this.retries = retries;
	}
	public int getTimeout() {
		return timeout;
	}
	public void setTimeout(int timeout) {
		this.timeout = timeout;
	}
	private Map<String, Object> config = new HashMap<String, Object>();
	
	public String getTopic() {
		return topic;
	}
	public void setTopic(String topic) {
		this.topic = topic;
	}
	public String getGroup() {
		return group;
	}
	public void setGroup(String group) {
		this.group = group;
	}
	public String getParamType() {
		return paramType;
	}
	public void setParamType(String paramType) {
		this.paramType = paramType;
	}
	public String getParentType() {
		return parentType;
	}
	public void setParentType(String parentType) {
		this.parentType = parentType;
	}
	public String getListenerClassName() {
		return listenerClassName;
	}
	public void setListenerClassName(String listenerClassName) {
		this.listenerClassName = listenerClassName;
	}
	public String getErrorHandler() {
		return errorHandler;
	}
	public void setErrorHandler(String errorHandler) {
		this.errorHandler = errorHandler;
	}
	public String getTargetMethod() {
		return targetMethod;
	}
	public void setTargetMethod(String targetMethod) {
		this.targetMethod = targetMethod;
	}
	
	
	public Map<String,Object> getConfig() {
		return config;
	}
	public void setConfig(Map<String, Object> config) {
		this.config = config;
	}
	public void setProperties(Map<String, Object> properties) {
		
		for(Map.Entry<String, Object> value : properties.entrySet()) {
			this.config.put(value.getKey(), value.getValue());
		}
	}
	
}
