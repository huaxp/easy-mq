package com.easy.mq.config.consumer;

import java.util.Map;

import org.apache.rocketmq.common.protocol.heartbeat.MessageModel;

import com.easy.mq.enums.OrderMode;

public class RocketConsumerConfig extends AbstractConsumerConfig{
	
	private String tag;
	private OrderMode consumeMode = OrderMode.NORMAL ;
	private String address;
	private Map<String,Object> properties;
	private MessageModel messageModel;
	private String instanceName;
	private int pullThresholdForQueue;
	private int pullInterval;
	private int persistConsumerOffsetInterval;
	private int consumeThreadMin;
	private int consumeThreadMax;
	private int consumeMessageBatchMaxSize;
	private String logLevel = "error";
	
	public MessageModel getMessageModel() {
		return messageModel;
	}
	public void setMessageModel(MessageModel messageModel) {
		this.messageModel = messageModel;
	}
	public String getInstanceName() {
		return instanceName;
	}
	public void setInstanceName(String instanceName) {
		this.instanceName = instanceName;
	}
	public int getPullThresholdForQueue() {
		return pullThresholdForQueue;
	}
	public void setPullThresholdForQueue(int pullThresholdForQueue) {
		this.pullThresholdForQueue = pullThresholdForQueue;
	}
	public int getPullInterval() {
		return pullInterval;
	}
	public void setPullInterval(int pullInterval) {
		this.pullInterval = pullInterval;
	}
	public int getPersistConsumerOffsetInterval() {
		return persistConsumerOffsetInterval;
	}
	public void setPersistConsumerOffsetInterval(int persistConsumerOffsetInterval) {
		this.persistConsumerOffsetInterval = persistConsumerOffsetInterval;
	}
	public int getConsumeThreadMin() {
		return consumeThreadMin;
	}
	public void setConsumeThreadMin(int consumeThreadMin) {
		this.consumeThreadMin = consumeThreadMin;
	}
	public int getConsumeThreadMax() {
		return consumeThreadMax;
	}
	public void setConsumeThreadMax(int consumeThreadMax) {
		this.consumeThreadMax = consumeThreadMax;
	}
	public int getConsumeMessageBatchMaxSize() {
		return consumeMessageBatchMaxSize;
	}
	public void setConsumeMessageBatchMaxSize(int consumeMessageBatchMaxSize) {
		this.consumeMessageBatchMaxSize = consumeMessageBatchMaxSize;
	}

	public String getTag() {
		return tag;
	}
	public void setTag(String tag) {
		this.tag = tag;
	}
	public OrderMode getConsumeMode() {
		return consumeMode;
	}
	public void setConsumeMode(OrderMode consumeMode) {
		this.consumeMode = consumeMode;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public Map<String, Object> getProperties() {
		return properties;
	}
	public void setProperties(Map<String, Object> properties) {
		this.properties = properties;
	}
	public String getLogLevel() {
		return logLevel;
	}
	public void setLogLevel(String logLevel) {
		this.logLevel = logLevel;
	}
	
	
}
