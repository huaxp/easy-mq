package com.easy.mq.config.producer;

public class RocketProducerConfig {
	private String group;
	private String instanceName;
	private String address;
	private String topic;
	private String tag;
	private int pollNameServerInteval;
	private int heartbeatBrokerInterval;
	private String serialization;
	
	private boolean enableLog = false;
	
	public int getPollNameServerInteval() {
		return pollNameServerInteval;
	}

	public void setPollNameServerInteval(int pollNameServerInteval) {
		this.pollNameServerInteval = pollNameServerInteval;
	}

	public String getTopic() {
		return topic;
	}

	public void setTopic(String topic) {
		this.topic = topic;
	}

	public String getTag() {
		return tag;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}

	public String getGroup() {
		return group;
	}

	public void setGroup(String group) {
		this.group = group;
	}

	public String getInstanceName() {
		return instanceName;
	}

	public void setInstanceName(String instanceName) {
		this.instanceName = instanceName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public int getHeartbeatBrokerInterval() {
		return heartbeatBrokerInterval;
	}

	public void setHeartbeatBrokerInterval(int heartbeatBrokerInterval) {
		this.heartbeatBrokerInterval = heartbeatBrokerInterval;
	}

	public String getSerialization() {
		return serialization;
	}

	public void setSerialization(String serialization) {
		this.serialization = serialization;
	}

	public boolean isEnableLog() {
		return enableLog;
	}

	public void setEnableLog(boolean enableLog) {
		this.enableLog = enableLog;
	}

}
