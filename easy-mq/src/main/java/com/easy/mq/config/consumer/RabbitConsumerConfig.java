package com.easy.mq.config.consumer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.easy.mq.enums.ConsumeMode;

public class RabbitConsumerConfig {
	private String address;
	private String username;
	private String password;
	private Integer port;
	private String queue;
	private String type;
	private Map<String, Object> headerMap = new HashMap<String, Object>();
	private List<String> queueBindList = new ArrayList<>();
	
	private boolean isAutoAck;
	private String exchange;
	private boolean durable = true;
	private boolean exclusive;
	private boolean autoDelete = true;
	private Integer prefetchCount = 1;
	private ConsumeMode consumeMode;
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public Integer getPort() {
		return port;
	}
	public void setPort(Integer port) {
		this.port = port;
	}
	public String getQueue() {
		return queue;
	}
	public void setQueue(String queue) {
		this.queue = queue;
	}
	
	public boolean isAutoAck() {
		return isAutoAck;
	}
	public void setAutoAck(boolean isAutoAck) {
		this.isAutoAck = isAutoAck;
	}
	public String getExchange() {
		return exchange;
	}
	public void setExchange(String exchange) {
		this.exchange = exchange;
	}
	public boolean isDurable() {
		return durable;
	}
	public void setDurable(boolean durable) {
		this.durable = durable;
	}
	public boolean isExclusive() {
		return exclusive;
	}
	public void setExclusive(boolean exclusive) {
		this.exclusive = exclusive;
	}
	public boolean isAutoDelete() {
		return autoDelete;
	}
	public void setAutoDelete(boolean autoDelete) {
		this.autoDelete = autoDelete;
	}
	public Integer getPrefetchCount() {
		return prefetchCount;
	}
	public void setPrefetchCount(Integer prefetchCount) {
		this.prefetchCount = prefetchCount;
	}
	public Map<String, Object> getHeaderMap() {
		return headerMap;
	}
	public void setHeaderMap(Map<String, Object> headerMap) {
		this.headerMap = headerMap;
	}
	public List<String> getQueueBindList() {
		return queueBindList;
	}
	public void setQueueBindList(List<String> queueBindList) {
		this.queueBindList = queueBindList;
	}
	public ConsumeMode getConsumeMode() {
		return consumeMode;
	}
	public void setConsumeMode(ConsumeMode consumeMode) {
		this.consumeMode = consumeMode;
	}
	
	
}
