package com.easy.mq.exception;

public class MQException extends RuntimeException {


	public MQException(String message) {
        super(message);
    }
	public MQException(String message, Throwable cause) {
        super(message, cause);
    }

	/**
	 * 
	 */
	private static final long serialVersionUID = -2601435832859119950L;

}
