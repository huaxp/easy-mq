package com.easy.mq.analysis;

import java.util.Map;

import org.apache.commons.lang.StringUtils;

/**
 * 
 * @author huaxp_cn@163.com
 * @desc $group.id[samp]
 */
public class AE {
	
	@SuppressWarnings("unchecked")
	public static String getValue(String key,Map<String, Object> properties) {
		if (StringUtils.isEmpty(key)) {
			return null;
		}
		if (key.indexOf("$") > -1) {
			final String innerKey = getInnerKey(key);
			if(StringUtils.isNotEmpty(innerKey)) {
				Object  innerValue = properties.get(getKey(key));
				if(innerValue != null && innerValue instanceof Map) {
					return String.valueOf(((Map<String,Object>)innerValue).get(innerKey));
				}
			}
			return String.valueOf(properties.get(getKey(key)));
		} else {
			return key;
		}
	}
	

	private static String getKey(String key) {
		String src = new String(key);
		src = src.replace("$", "");
		if (key.indexOf("[") > -1) {
			src = src.substring(0, key.indexOf("[") - 1);
		}
		return src;
	}
	
	private static String getInnerKey(String key) {
		if (key.indexOf("[") > -1 || key.indexOf("]") > -1) {
			return key.substring(key.indexOf("[")+1, key.indexOf("]"));
		}
		return "";
	}

}
