package com.easy.mq.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.easy.mq.enums.ConsumeMode;

@Retention(RetentionPolicy.RUNTIME)
@Target(value = { ElementType.METHOD, ElementType.FIELD })
public @interface RabbitListener {

	String queue() default "";
	
	String exchange() default "";
	
	String type() default "";
	
	String errorHandler() default "";
	
	ConsumeMode consumeMode() default ConsumeMode.PUSH;

}
