package com.easy.mq.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.easy.mq.enums.OrderMode;

@Retention(RetentionPolicy.RUNTIME)
@Target(value = { ElementType.METHOD, ElementType.FIELD })
public @interface RocketListener {

	String group() default "";

	String topic() default "";
	
	String tag() default "";
	
	String instanceName() default "";
	
	OrderMode consumeMode() default OrderMode.NORMAL;
	
	String errorHandler() default "";

}
