package com.easy.mq.result;

public class MQSubscribe  {
		
	private String topic;
	private String subExpression;
	private String tag;
	public String getTopic() {
		return topic;
	}
	public void setTopic(String topic) {
		this.topic = topic;
	}
	public String getSubExpression() {
		return subExpression;
	}
	public void setSubExpression(String subExpression) {
		this.subExpression = subExpression;
	}
	public String getTag() {
		return tag;
	}
	public void setTag(String tag) {
		this.tag = tag;
	}
	
}
