package com.easy.mq.result;

public class KafkaSendResult {

	private long offset;
	private int partition;
	private String topic;
	private long timestamp;

	public long getOffset() {
		return offset;
	}

	public void setOffset(long offset) {
		this.offset = offset;
	}

	public int getPartition() {
		return partition;
	}

	public void setPartition(int partition) {
		this.partition = partition;
	}

	public String getTopic() {
		return topic;
	}

	public void setTopic(String topic) {
		this.topic = topic;
	}

	public long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}
	
	/*public boolean isSuccess() {
		return "SEND_OK".equals(super.getSendStatus().name());
	}*/
	
	
}
