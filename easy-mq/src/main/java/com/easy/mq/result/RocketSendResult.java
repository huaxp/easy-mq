package com.easy.mq.result;

import org.apache.rocketmq.client.producer.SendResult;

public class RocketSendResult extends  SendResult {

	public boolean isSuccess() {
		return "SEND_OK".equals(super.getSendStatus().name());
	}
}
