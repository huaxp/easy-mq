package com.easy.mq.result;

import java.io.Serializable;

import org.apache.rocketmq.common.message.Message;

public class RocketProducerMessage extends Message implements Serializable {

	private static final long serialVersionUID = -2163315089030774299L;
	
	private String group;

	public String getGroup() {
		return group;
	}

	public void setGroup(String group) {
		this.group = group;
	}
	
	
}
