package com.easy.mq.result;

import java.io.Serializable;

public class RabbitProducerMessage implements Serializable {

	private static final long serialVersionUID = -2163315089030774299L;
	
	private String content;
	private String keys;
	private boolean async;
	private String queue;
	
	private boolean autoFlush = true;
	
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getKeys() {
		return keys;
	}
	public void setKeys(String keys) {
		this.keys = keys;
	}
	public boolean isAsync() {
		return async;
	}
	public void setAsync(boolean async) {
		this.async = async;
	}
	public boolean isAutoFlush() {
		return autoFlush;
	}
	public void setAutoFlush(boolean autoFlush) {
		this.autoFlush = autoFlush;
	}
	public String getQueue() {
		return queue;
	}
	public void setQueue(String queue) {
		this.queue = queue;
	}
	
}
