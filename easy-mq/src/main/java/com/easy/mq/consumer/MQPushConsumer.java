package com.easy.mq.consumer;

import org.apache.rocketmq.client.consumer.DefaultMQPushConsumer;
import org.apache.rocketmq.client.consumer.listener.MessageListenerConcurrently;
import org.apache.rocketmq.client.consumer.listener.MessageListenerOrderly;
import org.apache.rocketmq.client.exception.MQClientException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MQPushConsumer extends DefaultMQPushConsumer{
	
	private static final Logger log = LoggerFactory.getLogger(MQPushConsumer.class);
	
	  public MQPushConsumer(final String consumerGroup) {
	       super(consumerGroup);
	    }

	  @Override
	    public void registerMessageListener(MessageListenerConcurrently messageListener) {
		      super.registerMessageListener(messageListener);
		      try {
				super.start();
			} catch (MQClientException e) {
				e.printStackTrace();
			}
	        
	    }


	    @Override
	    public void registerMessageListener(MessageListenerOrderly messageListener) {
	    	super.registerMessageListener(messageListener);
	    	try {
				super.start();
				log.info("Register  event success [{}] .",this.getConsumerGroup());
			} catch (MQClientException e) {
				log.error(e.getMessage(),e);
			}
	    }
	  
	  
	
}
