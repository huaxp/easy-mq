package com.easy.mq.client;

import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeoutException;

import org.apache.rocketmq.client.exception.MQClientException;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

public final class RabbitProducerPool {
	private static Map<String, Channel> map = new ConcurrentHashMap<String, Channel>();

	public static Channel getProducer(Map<String, Object> config) throws MQClientException {
		if (map.size() > 20) {
			map.clear();
		}
		return null;
	}

	private static Channel mqConfig(Map<String, Object> config) {

		ConnectionFactory factory=new ConnectionFactory();
        factory.setHost(String.valueOf(config.get("address")));
        factory.setPort(Integer.parseInt(String.valueOf(config.get("port"))));
        Connection connection;
        Channel channel= null;
		try {
			connection = factory.newConnection();
			channel=connection.createChannel();
		} catch (IOException | TimeoutException e) {
			e.printStackTrace();
		}
        
		return channel;
	}

	public static void removePool(Map<String, Object> config) {
		Channel channel = map.get(config.get(KafkaClient.TOPIC));
		try {
			if (channel != null) {
				try {
					channel.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		} finally {
			map.remove(config.get(KafkaClient.TOPIC));
		}
	}
}
