/*package com.easy.mq.factory;

import java.lang.annotation.Annotation;
import java.util.Map;

import com.easy.mq.analysis.AE;
import com.easy.mq.annotation.RabbitListener;
import com.easy.mq.config.consumer.ManagerConsumerConfig;
import com.easy.mq.config.consumer.RabbitConsumerConfig;
import com.easy.mq.utils.MapUtil;
import com.easy.mq.utils.StringUtil;

public class RabbitConfigFactory extends AbstractMQConfigFactory<RabbitConsumerConfig> {

	RabbitConsumerConfig config = new RabbitConsumerConfig();

	public RabbitConfigFactory() {
		super(null,RabbitListener.class);
	}

	@Override
	public void addConsumerConfig(ManagerConsumerConfig<RabbitConsumerConfig> consumer,Annotation annotation)  {
		RabbitListener rabbitListener = (RabbitListener) annotation;
		consumer.setConsumer(config);
		Map<String,Object> map = MapUtil.objectToMap(consumer.getConsumer(), "");
		final String queue = String.valueOf(AE.getValue(rabbitListener.queue(),map));
		if(StringUtil.isNotEmpty(queue)) {
			config.setQueue(queue);
		}
		final String exchange = String.valueOf(AE.getValue(rabbitListener.exchange(),map));
		if(StringUtil.isNotEmpty(exchange)) {
			config.setExchange(exchange);
		}
		final String type = String.valueOf(AE.getValue(rabbitListener.type(),map));
		if(StringUtil.isNotEmpty(type)) {
			config.setType(type);
		}
		config.setConsumeMode(rabbitListener.consumeMode());
		
	}

	public RabbitConsumerConfig getConfig() {
		return config;
	}

	public void setConfig(RabbitConsumerConfig config) {
		this.config = config;
	}

	
}
*/