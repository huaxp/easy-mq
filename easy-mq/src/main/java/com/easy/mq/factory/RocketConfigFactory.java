package com.easy.mq.factory;

import java.lang.annotation.Annotation;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.easy.mq.analysis.AE;
import com.easy.mq.annotation.RocketListener;
import com.easy.mq.config.consumer.AbstractConsumerConfig;
import com.easy.mq.config.consumer.RocketConsumerConfig;

public class RocketConfigFactory extends AbstractMQConfigFactory<RocketConsumerConfig> {
	

	public RocketConfigFactory(Map<String,Object> config) {
		super(config);
	}

	@Override
	public AbstractConsumerConfig getConsumerConfig(Annotation annotation) {
		RocketConsumerConfig config = new RocketConsumerConfig();
		RocketListener rocketListener = (RocketListener) annotation;
		config.setAddress(MapUtils.getString(getConfig(),"address"));
		config.setTransfer(MapUtils.getString(getConfig(),"transfer"));		
		
		config.setConsumeMode(rocketListener.consumeMode());
		
		config.setTopic(AE.getValue(rocketListener.topic(),getConfig()));

		config.setGroup(AE.getValue(rocketListener.group(),getConfig()));

		config.setTag(AE.getValue(rocketListener.tag(),getConfig()));

		if (StringUtils.isEmpty(rocketListener.instanceName())) {
			config.setInstanceName(String.valueOf(System.currentTimeMillis()));
		}
		System.setProperty("rocketmq.client.logLevel", config.getLogLevel());
		return config;
		
	}
	
	@SuppressWarnings({"unchecked", "rawtypes"})
	@Override
	public Class getAnnotation() {
		return RocketListener.class;
	}

}
