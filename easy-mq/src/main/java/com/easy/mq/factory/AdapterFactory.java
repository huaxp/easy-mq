package com.easy.mq.factory;

import java.lang.annotation.Annotation;

import com.easy.mq.annotation.KafkaListener;
import com.easy.mq.annotation.RocketListener;
import com.easy.mq.listener.ManagerListener;
import com.easy.mq.listener.MessageListenerContainer;
import com.easy.mq.listener.adapter.KafkaListenerAdapter;
import com.easy.mq.listener.adapter.RocketListenerAdapter;

public class AdapterFactory<T> {
	
	public  MessageListenerContainer getListenerContainer(Annotation listener,ManagerListener<T> managerListener) {
		if(listener instanceof KafkaListener) {
			return  new KafkaListenerAdapter<T>(managerListener);
		}else if(listener instanceof RocketListener) {
			return  new RocketListenerAdapter<T>(managerListener);
		}
		return null;
	}
}
